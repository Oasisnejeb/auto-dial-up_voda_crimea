#! /usr/bin/python
# -*- coding: utf-8 -*-
VALID_PHONE_REGEXPS = ['+797', '7978', '8978', '+897']
VOICE_DIR = '/call_files/voice'
READY_DIR = '/call_files/ready'
TEXT_DIR = '/call_files/text'
OUTGOING_DIR = '/var/spool/asterisk/outgoing/'
NUMBERS_MATRIX = {
	1 : {
		'0' : '',
		'1' : 'одна',
		'2' : 'две',
		'3' : 'три',
		'4' : 'четыре',
		'5' : 'пять',
		'6' : 'шесть',
		'7' : 'семь',
		'8' : 'восемь',
		'9' : 'девять',
		},
        0 : { 
                '0' : '',
                '1' : 'десять',
                '2' : 'двaдцать',
                '3' : 'тридцать',
                '4' : 'сорок',
                '5' : 'пятьдесят',
                '6' : 'шестьдесят',
                '7' : 'семьдесят',
                '8' : 'восемьдесят',
                '9' : 'девяносто',
                }

	}
RUB_VALUE = 'рубль'.decode('utf8')
KOP_VALUE = 'копейка'.decode('utf8')
ASTERISK_DATA = {'Priority': '1','CallerID': 'VODA_CRIMEA','Extension': 's','WaitTime': '30','Context': 'gsm_outgoing','RetryTime': '300','MaxRetries': '0'}
