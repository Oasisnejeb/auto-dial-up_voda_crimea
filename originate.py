#! /usr/bin/python
import os,shutil
import call_settings
import pymorphy2
from decimal import Decimal


class DummyError():
	pass

def prepare_files(count):
	for filename in os.listdir(call_settings.READY_DIR)[:count]:
		cur_file = open('%s/%s' % (call_settings.READY_DIR, filename), 'r')
		content = cur_file.read()
		content = content + 'Channel: SIP/gsm-out/%s\n' % filename.split('.')[0]
		cur_file.close()
		new_file = open('%s/%s' % (call_settings.OUTGOING_DIR, filename), 'w+')
		new_file.write(content)
		new_file.close()
		os.unlink('%s/%s' % (call_settings.READY_DIR, filename))


prepare_files(1)
