#! /usr/bin/python
import os,shutil
import call_settings
import pymorphy2
from decimal import Decimal


class DummyError():
	pass

def get_line_contents(line):
	error = ''
	result = {}
	delimiter = line.find(';')
	if delimiter != -1:
		num = line[:delimiter]
		try:
			Decimal(num.replace(',', '.'))
		except:
			error = 'Num error - bad num '	+ num
			print error
		if not error:
			result['first_num'] = num.split(',')[0]
			try:
				result['last_num'] = num.split(',')[1]
			except:
				result['last_num'] = '0'
			tel = line[delimiter+1:].lstrip('+').rstrip('\r\n')
			if tel[:4] not in call_settings.VALID_PHONE_REGEXPS:
				error = 'Phone error - bad num '  + tel
			else:
				result['phone'] = tel
	if error:
		return (None,error)
	else:
		return (result,None)


def create_voice_file(call_data, dir = call_settings.VOICE_DIR):
	string = ' ' + call_data['first_num'] + ' ' + call_data['first_num_word'] +  ' ' + call_data['last_num'] + ' ' + call_data['last_num_word'] + '.'

        phone = call_data['phone']
        if phone[0] == '+':
                phone = '8' + phone[1:]
        if phone[0] == '7':
                phone = '8' + phone[1:]
	filepath = '%s/%s.wav' % (dir, phone)
	try:
		os.system('echo ' + string.encode('utf8') + ' | text2wave -o ' + filepath)
		os.system('sox  ' + filepath + ' -r 8000 -c 1 -t ul ' + '%s/%s.ulaw' % (dir, phone))
		os.unlink(filepath)
	except DummyError:
		return None
	return '%s/%s' % (dir, phone)

def create_call_file(call_data, asterisk_data = call_settings.ASTERISK_DATA, dir = call_settings.READY_DIR):
	if not call_data:
		return 0
	vfilepath = create_voice_file(call_data)
	contents = 'Set: VOICE_FILE=%s\n' % vfilepath
	for k,v in asterisk_data.iteritems():
		line = '%s: %s\n' % (k,v)
		contents += line
	phone = call_data['phone']
	if phone[0] == '+':
		phone = '8' + phone[1:]
	if phone[0] == '7':
		phone = '8' + phone[1:]
	filename = '%s.call' % phone
	filepath = '%s/%s' % (dir,filename)
	try:
		os.unlink(filepath)
	except:
		pass
	file = open(filepath, 'w')
	file.write(contents)
	file.close()
	return 1

def number_to_text_gent(number, matrix = call_settings.NUMBERS_MATRIX):
	if len(number) > 2 or number in ['10','11','12','13','14','15','16','17','18','19']:
		return number
	res_words = ' '
	for num,char in enumerate(number):
		res_words += matrix[int(num)][char]
		res_words += ' '
	return res_words

def parse_text_files(dir = call_settings.TEXT_DIR):
		morph = pymorphy2.MorphAnalyzer()
		rub = morph.parse(call_settings.RUB_VALUE)[0]
		kop = morph.parse(call_settings.KOP_VALUE)[0]
		for f in os.listdir(call_settings.TEXT_DIR):
			try:
				file = open('%s/%s' % (call_settings.TEXT_DIR,f))		
			except OSError:
				print 'Bad File ' + f
				continue
			for line in file.readlines():
				result,error = get_line_contents(line)
				if error:
					continue
				result['first_num_word'] = rub.make_agree_with_number(int(result['first_num'])).word
				result['last_num_word'] = kop.make_agree_with_number(int(result['last_num'])).word
				result['last_num'] = number_to_text_gent(result['last_num']).decode('utf8')
				res = create_call_file(result)
				print result
			os.unlink('%s/%s' % (call_settings.TEXT_DIR,f))

parse_text_files()
